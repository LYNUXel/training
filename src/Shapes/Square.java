//Created by LYNUX © 29.02.2020 at 15:27
package Shapes;

public class Square implements Shape{

    @Override
    public String draw() {
        return "Square";
    }

    @Override
    public double calculateArea() {
        return 0;
    }
}
