//Created by LYNUX © 29.02.2020 at 20:27
package Shapes;

public abstract class iShape {
    public abstract double calculateArea();
    public abstract double calculatePerimeter();
    public void cMethod(){
        System.out.println(" ... NONE ... ");
    }

}
