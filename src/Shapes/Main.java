//Created by LYNUX © 29.02.2020 at 15:24
package Shapes;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(10);
        System.out.println("Shape is: " + circle.draw());
        CircleWithBoarder newBoarder = new CircleWithBoarder(15);
        System.out.println(newBoarder.draw());

        Square square = new Square();
        System.out.println("Shape is: " + square.draw());
        SquareWithBoarder newBoarder1 = new SquareWithBoarder();
        System.out.println(newBoarder1.draw());

        Triangle triangle = new Triangle();
        System.out.println("Shape is: " + triangle.draw());
        TriangleWithBoarder newBoarder2 = new TriangleWithBoarder();
        System.out.println(newBoarder2.draw() + "\n");

        System.out.println(">>>>> Circles Area & Perimeter Calculation <<<<<");
        iShape[] shapes = generateShapes();
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].toString() + " => Area: " + shapes[i].calculateArea() + " and, \nPerimeter is : " + shapes[i].calculatePerimeter() + "\n");

        }
    }

    public static iShape[] generateShapes() {
        iShape[] shapes = new iShape[5];
        shapes[0] = new Circle(3);
        shapes[1] = new Circle(4);
        shapes[2] = new Circle(6);
        shapes[3] = new Circle(8);
        shapes[4] = new Circle(12);
        return shapes;
    }
//    or...
//    public static iShape[] generateShapes(int n) {
//        iShape[] shapes = new iShape[n];
//        for (int i = 0; i < n; i++) {
//            int rest = i % 3;
//
//            switch (rest) {
//                case 0:
//                case 1:
//                case 2:
//                    shapes[i] = new Circle(i);
//                    break;
//            }
//        }
//        return shapes;
//    }
}
