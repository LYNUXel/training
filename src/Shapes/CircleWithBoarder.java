//Created by LYNUX © 29.02.2020 at 15:35
package Shapes;

public class CircleWithBoarder extends Circle {
    public CircleWithBoarder(double diameter) {
        super(diameter);
    }

    @Override
    public String draw() {
        return "Black Boarder for " + super.draw();
    }
}
