//Created by LYNUX © 29.02.2020 at 17:24
package Shapes;

public class Triangle implements Shape{

    @Override
    public String draw() {
        return "Triangle";
    }

    @Override
    public double calculateArea() {
        return 0;
    }
}
