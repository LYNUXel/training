//Created by LYNUX © 29.02.2020 at 15:27
package Shapes;

public class Circle extends iShape implements Shape{
    private double diameter;

    public Circle (double diameter){
        this.diameter = diameter;
    }

    @Override
    public String toString(){
        return "Circle diameter is: " + diameter;
    }

    @Override
    public double calculateArea() {
        return (Math.PI * (diameter / 2) * (diameter / 2));
    }

    @Override
    public double calculatePerimeter() {
        return (2 * Math.PI * diameter / 2);
    }

    @Override
    public String draw() {
        return "Circle";
    }
}
