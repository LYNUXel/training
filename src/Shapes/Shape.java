//Created by LYNUX © 29.02.2020 at 15:25
package Shapes;

public interface Shape {
    String draw();

    double calculateArea();
}
